# Copyright 2014, Red Hat, Inc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Authors:
#    Josef Skladanka <jskladan@redhat.com>

# This is required for running on EL6
import __main__
__main__.__requires__ = ['SQLAlchemy >= 0.7', 'Flask >= 0.9', 'jinja2 >= 2.6']
import pkg_resources


from optparse import OptionParser
import sys
import os

from execdb import db
from execdb.models.user import User
from execdb.models.job import Job, BuildStep

from alembic.config import Config
from alembic import command as al_command
from alembic.migration import MigrationContext

from sqlalchemy.engine import reflection


def get_alembic_config():
    # the location of the alembic ini file and alembic scripts changes when
    # installed via package
    if os.path.exists("./alembic.ini"):
        alembic_cfg = Config("./alembic.ini")
    else:
        alembic_cfg = Config("/usr/share/execdb/alembic.ini",
                             ini_section='alembic-packaged')
    return alembic_cfg


def upgrade_db(*args):
    print "Upgrading Database to Latest Revision"
    alembic_cfg = get_alembic_config()
    al_command.upgrade(alembic_cfg, "head")


def init_alembic(*args):
    alembic_cfg = get_alembic_config()

    # check to see if the db has already been initialized by checking for an
    # alembic revision
    context = MigrationContext.configure(db.engine.connect())
    current_rev = context.get_current_revision()

    if not current_rev:
        print "Initializing alembic"
        print " - Setting the current version to the first revision"
        al_command.stamp(alembic_cfg, "1cefaba53e0")
    else:
        print "Alembic already initialized"


def initialize_db(destructive):
    alembic_cfg = get_alembic_config()

    print "Initializing database"

    if destructive:
        print " - Dropping all tables"
        db.drop_all()

    # check whether the table 'job' exists
    # if it doesn't, we assume that the database is empty
    insp = reflection.Inspector.from_engine(db.engine)
    table_names = insp.get_table_names()
    if 'job' not in table_names and 'Job' not in table_names:
        print " - Creating tables"
        db.create_all()
        print " - Stamping alembic's current version to 'head'"
        al_command.stamp(alembic_cfg, "head")

    # check to see if the db has already been initialized by checking for an
    # alembic revision
    context = MigrationContext.configure(db.engine.connect())
    current_rev = context.get_current_revision()
    if current_rev:
        print " - Database is currently at rev %s" % current_rev
        upgrade_db(destructive)
    else:
        print "WARN: You need to have your db stamped with an alembic revision"
        print "      Run 'init_alembic' sub-command first."


def mock_data(destructive):
    print "Populating tables with mock-data"

    if destructive or not db.session.query(User).count():
        print " - User"
        data_users = [('admin', 'admin'), ('user', 'user')]

        for d in data_users:
            u = User(*d)
            db.session.add(u)

        db.session.commit()
    else:
        print " - skipped User"


def main():
    possible_commands = ['init_db', 'mock_data', 'upgrade_db', 'init_alembic']

    usage = 'usage: [DEV=true] %prog ' + "(%s)" % ' | '.join(possible_commands)
    parser = OptionParser(usage=usage)
    parser.add_option("-d", "--destructive",
                      action="store_true", dest="destructive", default=False,
                      help="Drop tables in `init_db`; Store data in `mock_data` "
                      "even if the tables are not empty")

    (options, args) = parser.parse_args()

    if len(args) != 1 or args[0] not in possible_commands:
        print usage
        print
        print 'Please use one of the following commands: %s' % str(possible_commands)
        sys.exit(1)

    command = {
        'init_db': initialize_db,
        'mock_data': mock_data,
        'upgrade_db': upgrade_db,
        'init_alembic': init_alembic,
    }[args[0]]
    if not options.destructive:
        print "Proceeding in non-destructive mode. To perform destructive "\
              "steps use -d option."

    command(options.destructive)

    sys.exit(0)


if __name__ == '__main__':
    main()
