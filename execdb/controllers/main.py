# Copyright 2014, Red Hat, Inc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Authors:
#    Josef Skladanka <jskladan@redhat.com>

from flask import Blueprint, render_template, request, jsonify
import werkzeug.exceptions
from sqlalchemy.orm import exc as orm_exc

from flask.ext.restful import reqparse
from werkzeug.exceptions import HTTPException
from werkzeug.exceptions import BadRequest as JSONBadRequest


from execdb import app, db
from execdb.models.job import Job, BuildStep
from sqlalchemy import desc

import json
import re

from pprint import pformat

main = Blueprint('main', __name__)
BB_URL = app.config['BUILDBOT_FRONTPAGE_URL']
RESULTSDB_URL = app.config['RESULTSDB_FRONTPAGE_URL']

RE_PAGE = re.compile(r"([?&])page=([0-9]+)")
RP = {}
RP['get_jobs'] = reqparse.RequestParser()
RP['get_jobs'].add_argument('page', default=0, type=int, location='args')
RP['get_jobs'].add_argument('limit', default=30, type=int, location='args')


def pagination(q, page, limit):
    # pagination offset
    try:
        page = int(page)
        if page > 0:
            offset = page * limit
            q = q.offset(offset)
    except (TypeError, ValueError):
        pass

    # apply the query limit
    try:
        limit = int(limit)
    except (ValueError, TypeError):
        limit = QUERY_LIMIT

    q = q.limit(limit)
    return q

# TODO: find a better way to do this


def prev_next_urls():
    global RE_PAGE
    try:
        match = RE_PAGE.findall(request.url)
        flag, page = match[0][0], int(match[0][1])
    except IndexError:  # page not found
        if '?' in request.url:
            return None, "%s&page=1" % request.url
        else:
            return None, "%s?page=1" % request.url

    prev = None
    next = None
    prevpage = page - 1
    nextpage = page + 1

    if page > 0:
        prev = RE_PAGE.sub("%spage=%s" % (flag, prevpage), request.url)
    next = RE_PAGE.sub("%spage=%s" % (flag, nextpage), request.url)

    return prev, next


@main.route('/')
@main.route('/index')
@main.route('/jobs', methods=['GET'])
def index():
    try:
        args = RP['get_jobs'].parse_args()
    except JSONBadRequest as error:
        return jsonify({"message": "Bad Request"}), error.code
    except HTTPException as error:
        return jsonify(error.data), error.code

    query = db.session.query(Job).order_by(desc(Job.t_triggered))
    query = pagination(query, args['page'], args['limit'])

    prev, next = prev_next_urls()
    jobs = query.all()

    return render_template('index.html',
                           jobs=jobs,
                           buildbot_url=BB_URL,
                           prev=prev,
                           next=next)


@main.route('/jobs/<uuid>', methods=['GET'])
def show_job(uuid):
    try:
        job = db.session.query(Job).filter(Job.uuid == uuid).one()
    except orm_exc.NoResultFound:
        return 'UUID not found', 404

    return render_template('show_job.html',
                           job=job,
                           buildbot_url=BB_URL,
                           resultsdb_url=RESULTSDB_URL,
                           artifacts_base_url=app.config['ARTIFACTS_BASE_URL'])


@main.route('/jobs', methods=['POST'])
def create_job():
    job = Job()

    data = request.json
    job.fedmsg_data = json.dumps(data)

    # FIXME - add validation
    job.taskname = data['taskname']
    job.item = data['item']
    job.item_type = data['item_type']
    job.arch = data['arch']

    db.session.add(job)
    db.session.commit()
    # FIXME - add resultsdb-like serializer
    retval = {
        'id': job.id,
        'uuid': job.uuid,
        't_triggered': job.t_triggered.isoformat()
    }

    return jsonify(retval), 201


def process_event(data):

    def bb_convert_properties(prop):
        """Converts list of lists to dict"""
        return dict([(key, value) for key, value, _ in prop])

    # at the moment, we act just on these events
    event = data['event']
    known_events = ['changeAdded', 'buildStarted', 'stepStarted',
                    'stepFinished', 'buildFinished']

    if event not in known_events:
        # FIXME remove
        if 'uuid' in json.dumps(data):
            app.logger.debug("UUID found in %s", event)

        return 'Skipping event', 204

    # grab the 'properties' field
    if event == 'changeAdded':
        properties = bb_convert_properties(data['payload']['change']['properties'])
    elif event in ['buildStarted', 'buildFinished']:
        properties = bb_convert_properties(data['payload']['build']['properties'])
    elif event in ['stepStarted', 'stepFinished']:
        properties = bb_convert_properties(data['payload']['properties'])

    # abort if uuid is not provided
    try:
        uuid = properties['uuid']
    except KeyError:
        return 'Missing `uuid` field in properties', 400

    if uuid is None:
        return 'UUID set to None', 400

    try:
        job = db.session.query(Job).filter(Job.uuid == uuid).one()
    except orm_exc.NoResultFound:
        return 'UUID not found', 400

    if event == 'changeAdded':
        # FIXME ?
        pass

    elif event == 'buildStarted' and job.current_state == 'Triggered':
        job.start()

        job.taskname = properties['taskname']
        job.item = properties['item']
        job.item_type = properties['item_type']
        job.arch = properties['arch']
        job.slavename = properties['slavename']
        job.link_build_log = '/builders/%s/builds/%s' % (
            data['payload']['build']['builderName'],
            properties['buildnumber'])

        db.session.add(job)

        # add 'empty' steps for the build (since we know them already)
#        app.logger.debug("%s: %s" % (uuid, data['payload']['build']['steps']))
#        app.logger.debug("%s - Build Started" % uuid)
        for step_info in data['payload']['build']['steps']:
            #            app.logger.debug("%s -- adding step %s"% (uuid, step_info['name']))
            step = BuildStep(name=step_info['name'])
            step.job = job
            db.session.add(step)

        db.session.commit()

    elif event == 'stepStarted' and job.current_state == 'Running':
        step_info = data['payload']['step']
#        app.logger.debug("%s - Step Started -  %s"% (uuid, step_info['name']))
        try:
            step = job.get_build_step(step_info['name'])
        except KeyError:
            app.logger.debug("Job %s had missing step %s", job.uuid, step_info)
            step = BuildStep(name=step_info['name'])
            step.job = job

        step.start()
        step.status = 'INPROGRESS'
        step.data = json.dumps(data['payload'])  # FIXME - store sensible subset of data
        db.session.add(step)
        db.session.commit()
#        app.logger.debug("%s - Step Started -  %s - written to db"% (uuid, step_info['name']))

    elif event == 'stepFinished' and job.current_state == 'Running':
        step_info = data['payload']['step']
#        app.logger.debug("%s - Step Finished -  %s"% (uuid, step_info['name']))
        try:
            step = job.get_build_step(step_info['name'])
        except KeyError:
            return 'StepFinished received for non-existing step: %r' % step_info['name'], 400

        step.finish()

        step.status = 'OK'
        # results key is only present for non-ok results
        if 'results' in step_info.keys():
            step.status = 'NOT OK'
        step.data = json.dumps(data['payload'])  # FIXME - store sensible subset of data

        db.session.add(step)
        db.session.commit()
#        app.logger.debug("%s - Step Finished -  %s - written to db" % (uuid, step_info['name']))

    elif event == 'buildFinished' and job.current_state == 'Running':
        job.finish()
        db.session.add(job)
        db.session.commit()
#        app.logger.debug("%s - Build Finished " % uuid)


@main.route('/buildbottest', methods=['POST'])
def bb_push():
    """
    Receives the post-push notifications from buildbot and fills in
    the steps for the job.
    """
    # data are embedded in form field 'packets'
    data = request.form
    try:
        data = request.form['packets']
    except werkzeug.exceptions.BadRequestKeyError:
        return 'Field `packets` missing in request form.', 400
    data = json.loads(data)

    # app.logger.debug(pformat(data))

    # multiple messages may be present in one 'packet'
    for entry in data:
        process_event(entry)
#        app.logger.debug("%s %s, %s", entry['id'], entry['event'], process_event(entry))

    # plain 200 code needs to be returned - otherwise buildbot is
    # endlessly trying to re-send the message.
    # FIXME - add logging for non-200 responses
    return '', 200
