# Copyright 2014, Red Hat, Inc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Authors:
#    Josef Skladanka <jskladan@redhat.com>

from execdb import db, app

import datetime
import uuid


class Job(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(36), unique=True)

    t_triggered = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    t_build_started = db.Column(db.DateTime)
    t_build_ended = db.Column(db.DateTime)

    fedmsg_data = db.Column(db.Text)

    taskname = db.Column(db.String(20))
    item = db.Column(db.Text)
    item_type = db.Column(db.String(20))
    arch = db.Column(db.String(10))

    slavename = db.Column(db.String(20))
    # slave_env_details = db.Column(db.Text) # ???

    # task_git_hash = db.Column(db.String(40)) # ???

    # link_machine_log = db.Column(db.Text) # ???
    link_build_log = db.Column(db.Text)  # $buildbot_url/builders/$builder_name/builds/$build_id
    # link_resultsdb = db.Column(db.Text) # Should be UUID-based

    build_steps = db.relation('BuildStep', backref='job', order_by="BuildStep.id")

    __table_args__ = (
        db.Index('job_idx_item', 'item',
                 postgresql_ops={'item': 'text_pattern_ops'},
                 ),
        db.Index('job_idx_taskname', 'taskname',
                 postgresql_ops={'taskname': 'text_pattern_ops'},
                 ),
    )

    def __init__(self, fedmsg_data=None):
        self.uuid = str(uuid.uuid1())
        self.fedmsg_data = fedmsg_data

    def start(self):
        self.t_build_started = datetime.datetime.utcnow()

    def finish(self):
        self.t_build_ended = datetime.datetime.utcnow()

    @property
    def current_state(self):
        if self.t_build_started and not self.t_build_ended:
            return "Running"
        if self.t_build_ended:
            return "Finished"
        return "Triggered"

    @property
    def started_after(self):
        try:
            return self.t_build_started - self.t_triggered
        except TypeError:
            return None

    @property
    def build_took(self):
        try:
            return self.t_build_ended - self.t_build_started
        except TypeError:
            return None

    def get_build_step(self, name):
        for step in self.build_steps:
            if step.name == name:
                return step
        raise KeyError("Step %r not found" % name)


class BuildStep(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    job_id = db.Column(db.Integer, db.ForeignKey('job.id'))
    name = db.Column(db.String(20))
    status = db.Column(db.String(10))
    started_at = db.Column(db.DateTime)
    finished_at = db.Column(db.DateTime)
    data = db.Column(db.Text)

    __table_args__ = (
        db.Index('buildstep_idx_status', 'status',
                 postgresql_ops={'status': 'text_pattern_ops'},
                 ),
        db.Index('buildstep_fk_job_id', 'job_id'),
    )

    def __repr__(self):
        return "(%s, %s)" % (self.name, self.status)

    def __init__(self, name):
        self.name = name

    def start(self):
        self.started_at = datetime.datetime.utcnow()

    def finish(self):
        self.finished_at = datetime.datetime.utcnow()

    @property
    def log_url(self):
        return "%s/steps/%s" % (self.job.link_build_log, self.name)
