# Copyright 2014, Red Hat, Inc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Authors:
#    Josef Skladanka <jskladan@redhat.com>


class Config(object):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'

    LOGFILE = '/var/log/execdb/execdb.log'
    FILE_LOGGING = False
    SYSLOG_LOGGING = False
    STREAM_LOGGING = True

    RUN_HOST = '0.0.0.0'
    RUN_PORT = 5003

    PRODUCTION = False
    TESTING = False

    SHOW_DB_URI = False

    BUILDBOT_FRONTPAGE_URL = 'http://taskotron-local/taskmaster'
    RESULTSDB_FRONTPAGE_URL = 'http://taskotron-local/resultsdb'
    ARTIFACTS_BASE_URL = 'http://taskotron-local/artifacts/all'


class ProductionConfig(Config):
    DEBUG = False
    PRODUCTION = True


class DevelopmentConfig(Config):
    TRAP_BAD_REQUEST_ERRORS = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:////var/tmp/execdb_db.sqlite'
    SHOW_DB_URI = True


class TestingConfig(Config):
    TRAP_BAD_REQUEST_ERRORS = True
    TESTING = True
