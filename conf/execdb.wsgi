# This is required for running on EL6
import __main__
__main__.__requires__ = ['SQLAlchemy >= 0.7', 'Flask >= 0.9', 'jinja2 >= 2.6']
import pkg_resources

# if you're running the app from a virtualenv, uncomment these lines
#activate_this = '/var/www/execdb/env/bin/activate_this.py'
#execfile(activate_this, dict(__file__=activate_this))
#import sys
#sys.path.insert(0,"/var/www/execdb/execdb/")

import os
os.environ['EXECDB_CONFIG'] = '/etc/execdb/settings.py'

from execdb import app as application
