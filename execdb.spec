%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif

Name:           execdb
Version:        0.0.7
Release:        1%{?dist}
Summary:        Execution status database for Taskotron

License:        GPLv2+
URL:            https://bitbucket.org/fedoraqa/execdb
Source0:        https://qadevel.cloud.fedoraproject.org/releases/%{name}/%{name}-%{version}.tar.gz

BuildArch:      noarch

Requires:       python-flask
Requires:       python-flask-sqlalchemy
Requires:       python-flask-wtf
Requires:       python-flask-login
Requires:       python-flask-restful
Requires:       python-six
Requires:       python-alembic
BuildRequires:  python2-devel python-setuptools

%description
ExecDB is a execution status database for Taskotron

%prep
%setup -q

%build
%{__python2} setup.py build

%install
%{__python2} setup.py install --skip-build --root %{buildroot}

# apache and wsgi settings
mkdir -p %{buildroot}%{_datadir}/execdb/conf
cp conf/execdb.conf %{buildroot}%{_datadir}/execdb/conf/.
cp conf/execdb.wsgi %{buildroot}%{_datadir}/execdb/.

# alembic config and data
cp -r alembic %{buildroot}%{_datadir}/execdb/.
install alembic.ini %{buildroot}%{_datadir}/execdb/.

# exedb config
mkdir -p %{buildroot}%{_sysconfdir}/execdb
install conf/settings.py.example %{buildroot}%{_sysconfdir}/execdb/settings.py.example

%files
%doc README.md conf/*
%{python_sitelib}/execdb
%{python_sitelib}/*.egg-info

%attr(755,root,root) %{_bindir}/execdb
%dir %{_sysconfdir}/execdb
%{_sysconfdir}/execdb/*
%dir %{_datadir}/execdb
%{_datadir}/execdb/*

%changelog
* Wed Jun 17 2015 Josef Skladanka <jskladan@fedoraproject.org> - 0.0.7
- added alembic config and data to package
- added requires python-alembic

* Mon Mar 30 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.6
- bumped version for initial release

* Thu Feb 12 2015 Josef Skladanka <jskladan@redhat.com> - 0.0.1
- initial packaging
