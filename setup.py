from setuptools import setup
import codecs
import re
import os

here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    return codecs.open(os.path.join(here, *parts), 'r').read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")

setup(name='execdb',
      version=find_version('execdb', '__init__.py'),
      description='Execution Database',
      author='Josef Skladanka',
      author_email='jskladan@redhat.com',
      license='GPLv2+',
      packages=['execdb', 'execdb.controllers', 'execdb.models'],
      package_dir={'execdb': 'execdb'},
      entry_points=dict(console_scripts=['execdb=execdb.cli:main']),
      include_package_data=True,
      )
