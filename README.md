# ExecDB

ExecDB is a database that stores the execution status of jobs running inside
the Taskotron framework. You can see which jobs were scheduled, started and
finished, and some of their properties.

The API is currently not documented, but you can see how it is used by looking
into the [taskotron-trigger](https://bitbucket.org/fedoraqa/taskotron-trigger)
project.

## Quick development setup

First, clone the repository.

Then, setup a virtual environment for development:

    $ sudo dnf install python-virtualenv
    $ virtualenv env_execdb
    $ source env_execdb/bin/activate
    $ pip install -r requirements.txt

Initialize your database:

    $ DEV=true ./init_db.sh

Run the server:

    $ DEV=true python runapp.py

The server is now running with its frontend available at
<http://localhost:5003>. All data is stored inside `/var/tmp/execdb_db.sqlite`.

## Adjusting configuration

You can configure this app by copying `conf/settings.py.example` into
`conf/setting.py` and adjusting values as you see fit. It overrides default
values in `execdb/config.py`.

## Using with Taskotron

You might want to use this tool together with libtaskotron. To use your own
*ExecDB* server in libtaskotron, edit `/etc/taskotron/taskotron.yaml` and
set the following value::

    execdb_server: http://localhost:5003

In taskotron-trigger, you'll find the same option in
`/etc/taskotron/trigger.cfg`.
